/* A Bison parser, made by GNU Bison 2.7.  */

/* Skeleton interface for Bison GLR parsers in C++
   
      Copyright (C) 2002-2006, 2009-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C++ GLR parser skeleton written by Akim Demaille.  */

#ifndef YY_YY_WORKSPACE_TEST_OOOOOOOOOO_OUT_SOONG_INTERMEDIATES_SYSTEM_TOOLS_AIDL_LIBAIDL_COMMON_LINUX_GLIBC_X86_64_STATIC_GEN_YACC_SYSTEM_TOOLS_AIDL_AIDL_LANGUAGE_Y_H_INCLUDED
# define YY_YY_WORKSPACE_TEST_OOOOOOOOOO_OUT_SOONG_INTERMEDIATES_SYSTEM_TOOLS_AIDL_LIBAIDL_COMMON_LINUX_GLIBC_X86_64_STATIC_GEN_YACC_SYSTEM_TOOLS_AIDL_AIDL_LANGUAGE_Y_H_INCLUDED



# include <string>
# include <iostream>
# include "location.hh"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif


namespace yy {
/* Line 250 of glr.cc  */
#line 53 "/workspace/test/oooooooooo/out/soong/.intermediates/system/tools/aidl/libaidl-common/linux_glibc_x86_64_static/gen/yacc/system/tools/aidl/aidl_language_y.h"
  /// A Bison parser.
  class parser
  {
  public:
    /// Symbol semantic values.
# ifndef YYSTYPE
    union semantic_type
    {
/* Line 257 of glr.cc  */
#line 45 "system/tools/aidl/aidl_language_y.yy"

    AidlToken* token;
    char character;
    std::string *str;
    AidlAnnotation* annotation;
    std::vector<AidlAnnotation>* annotation_list;
    AidlTypeSpecifier* type;
    AidlArgument* arg;
    AidlArgument::Direction direction;
    AidlConstantValue* constant_value;
    std::vector<std::unique_ptr<AidlConstantValue>>* constant_value_list;
    std::vector<std::unique_ptr<AidlArgument>>* arg_list;
    AidlVariableDeclaration* variable;
    std::vector<std::unique_ptr<AidlVariableDeclaration>>* variable_list;
    AidlMethod* method;
    AidlMember* constant;
    std::vector<std::unique_ptr<AidlMember>>* interface_members;
    AidlQualifiedName* qname;
    AidlInterface* interface;
    AidlParcelable* parcelable;
    AidlDefinedType* declaration;
    std::vector<std::unique_ptr<AidlTypeSpecifier>>* type_args;


/* Line 257 of glr.cc  */
#line 89 "/workspace/test/oooooooooo/out/soong/.intermediates/system/tools/aidl/libaidl-common/linux_glibc_x86_64_static/gen/yacc/system/tools/aidl/aidl_language_y.h"
    };
# else
    typedef YYSTYPE semantic_type;
# endif
    /// Symbol locations.
    typedef location location_type;
    /// Tokens.
    struct token
    {
      /* Tokens.  */
   enum yytokentype {
     ANNOTATION = 258,
     C_STR = 259,
     IDENTIFIER = 260,
     INTERFACE = 261,
     PARCELABLE = 262,
     ONEWAY = 263,
     CHARVALUE = 264,
     FLOATVALUE = 265,
     HEXVALUE = 266,
     INTVALUE = 267,
     CONST = 268,
     UNKNOWN = 269,
     CPP_HEADER = 270,
     IMPORT = 271,
     IN = 272,
     INOUT = 273,
     OUT = 274,
     PACKAGE = 275,
     TRUE_LITERAL = 276,
     FALSE_LITERAL = 277
   };

    };
    /// Token type.
    typedef token::yytokentype token_type;

    /// Build a parser object.
    parser (Parser* ps_yyarg);
    virtual ~parser ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

    /// The current debugging stream.
    std::ostream& debug_stream () const;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);

  private:

  public:
    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);
  private:

# if YYDEBUG
  public:
    /// \brief Report a symbol value on the debug stream.
    /// \param yytype       The token type.
    /// \param yyvaluep     Its semantic value.
    /// \param yylocationp  Its location.
    virtual void yy_symbol_value_print_ (int yytype,
                                         const semantic_type* yyvaluep,
                                         const location_type* yylocationp);
    /// \brief Report a symbol on the debug stream.
    /// \param yytype       The token type.
    /// \param yyvaluep     Its semantic value.
    /// \param yylocationp  Its location.
    virtual void yy_symbol_print_ (int yytype,
                                   const semantic_type* yyvaluep,
                                   const location_type* yylocationp);
  private:
    /* Debugging.  */
    std::ostream* yycdebug_;
# endif


    /* User arguments.  */
    Parser* ps;
  };



#ifndef YYSTYPE
# define YYSTYPE yy::parser::semantic_type
#endif
#ifndef YYLTYPE
# define YYLTYPE yy::parser::location_type
#endif


} // yy
/* Line 343 of glr.cc  */
#line 194 "/workspace/test/oooooooooo/out/soong/.intermediates/system/tools/aidl/libaidl-common/linux_glibc_x86_64_static/gen/yacc/system/tools/aidl/aidl_language_y.h"

#endif /* !YY_YY_WORKSPACE_TEST_OOOOOOOOOO_OUT_SOONG_INTERMEDIATES_SYSTEM_TOOLS_AIDL_LIBAIDL_COMMON_LINUX_GLIBC_X86_64_STATIC_GEN_YACC_SYSTEM_TOOLS_AIDL_AIDL_LANGUAGE_Y_H_INCLUDED  */
