#ifndef AIDL_GENERATED_ANDROID_SECURITY_BP_CONFIRMATION_PROMPT_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_BP_CONFIRMATION_PROMPT_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/security/IConfirmationPromptCallback.h>

namespace android {

namespace security {

class BpConfirmationPromptCallback : public ::android::BpInterface<IConfirmationPromptCallback> {
public:
  explicit BpConfirmationPromptCallback(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpConfirmationPromptCallback() = default;
  ::android::binder::Status onConfirmationPromptCompleted(int32_t result, const ::std::vector<uint8_t>& dataThatWasConfirmed) override;
};  // class BpConfirmationPromptCallback

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_BP_CONFIRMATION_PROMPT_CALLBACK_H_
