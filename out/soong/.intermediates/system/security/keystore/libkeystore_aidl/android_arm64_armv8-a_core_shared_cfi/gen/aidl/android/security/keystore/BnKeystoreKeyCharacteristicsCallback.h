#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BN_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BN_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_

#include <binder/IInterface.h>
#include <android/security/keystore/IKeystoreKeyCharacteristicsCallback.h>

namespace android {

namespace security {

namespace keystore {

class BnKeystoreKeyCharacteristicsCallback : public ::android::BnInterface<IKeystoreKeyCharacteristicsCallback> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnKeystoreKeyCharacteristicsCallback

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BN_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_
