#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_BP_DROP_BOX_MANAGER_SERVICE_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_BP_DROP_BOX_MANAGER_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <com/android/internal/os/IDropBoxManagerService.h>

namespace com {

namespace android {

namespace internal {

namespace os {

class BpDropBoxManagerService : public ::android::BpInterface<IDropBoxManagerService> {
public:
  explicit BpDropBoxManagerService(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpDropBoxManagerService() = default;
  ::android::binder::Status add(const ::android::os::DropBoxManager::Entry& entry) override;
  ::android::binder::Status isTagEnabled(const ::android::String16& tag, bool* _aidl_return) override;
  ::android::binder::Status getNextEntry(const ::android::String16& tag, int64_t millis, const ::android::String16& packageName, ::android::os::DropBoxManager::Entry* _aidl_return) override;
};  // class BpDropBoxManagerService

}  // namespace os

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_BP_DROP_BOX_MANAGER_SERVICE_H_
