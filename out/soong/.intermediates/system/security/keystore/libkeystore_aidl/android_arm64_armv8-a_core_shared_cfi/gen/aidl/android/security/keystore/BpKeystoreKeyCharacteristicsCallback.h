#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/security/keystore/IKeystoreKeyCharacteristicsCallback.h>

namespace android {

namespace security {

namespace keystore {

class BpKeystoreKeyCharacteristicsCallback : public ::android::BpInterface<IKeystoreKeyCharacteristicsCallback> {
public:
  explicit BpKeystoreKeyCharacteristicsCallback(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpKeystoreKeyCharacteristicsCallback() = default;
  ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response, const ::android::security::keymaster::KeyCharacteristics& charactersistics) override;
};  // class BpKeystoreKeyCharacteristicsCallback

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_
