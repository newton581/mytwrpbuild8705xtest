#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_CERTIFICATE_CHAIN_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_CERTIFICATE_CHAIN_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/security/keystore/IKeystoreCertificateChainCallback.h>

namespace android {

namespace security {

namespace keystore {

class BpKeystoreCertificateChainCallback : public ::android::BpInterface<IKeystoreCertificateChainCallback> {
public:
  explicit BpKeystoreCertificateChainCallback(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpKeystoreCertificateChainCallback() = default;
  ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response, const ::android::security::keymaster::KeymasterCertificateChain& chain) override;
};  // class BpKeystoreCertificateChainCallback

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_CERTIFICATE_CHAIN_CALLBACK_H_
