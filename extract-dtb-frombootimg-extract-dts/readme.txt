wget https://www.kernel.org/pub/software/utils/dtc/dtc-1.6.1.tar.xz
make

wget https://gitlab.com/newton581/stockromm8fhd/-/raw/main/TB_8705X_S300219_210713_BMP/boot.img
pip install extract-dtb
extract-dtb -o output/ boot.img

../dtc-1.6.1/fdtdump 01_dtbdump_MT6765.dtb > 01_dtbdump_MT6765.dtb.txt
../dtc-1.6.1/fdtdump 02_dtbdump_MT6765.dtb > 02_dtbdump_MT6765.dtb.txt
../dtc-1.6.1/dtc -I dtb -O dts -o 01_dtbdump_MT6765.dts 01_dtbdump_MT6765.dtb &> 01_dtbdump_MT6765.dts.error
../dtc-1.6.1/dtc -I dtb -O dts -o 02_dtbdump_MT6765.dts 02_dtbdump_MT6765.dtb &> 02_dtbdump_MT6765.dts.error
