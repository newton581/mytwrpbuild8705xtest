#ifndef AIDL_GENERATED_ANDROID_OS_I_VOLD_LISTENER_H_
#define AIDL_GENERATED_ANDROID_OS_I_VOLD_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>
#include <utils/StrongPointer.h>

namespace android {

namespace os {

class IVoldListener : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(VoldListener)
  virtual ::android::binder::Status onDiskCreated(const ::std::string& diskId, int32_t flags) = 0;
  virtual ::android::binder::Status onDiskScanned(const ::std::string& diskId) = 0;
  virtual ::android::binder::Status onDiskMetadataChanged(const ::std::string& diskId, int64_t sizeBytes, const ::std::string& label, const ::std::string& sysPath) = 0;
  virtual ::android::binder::Status onDiskDestroyed(const ::std::string& diskId) = 0;
  virtual ::android::binder::Status onVolumeCreated(const ::std::string& volId, int32_t type, const ::std::string& diskId, const ::std::string& partGuid) = 0;
  virtual ::android::binder::Status onVolumeStateChanged(const ::std::string& volId, int32_t state) = 0;
  virtual ::android::binder::Status onVolumeMetadataChanged(const ::std::string& volId, const ::std::string& fsType, const ::std::string& fsUuid, const ::std::string& fsLabel) = 0;
  virtual ::android::binder::Status onVolumePathChanged(const ::std::string& volId, const ::std::string& path) = 0;
  virtual ::android::binder::Status onVolumeInternalPathChanged(const ::std::string& volId, const ::std::string& internalPath) = 0;
  virtual ::android::binder::Status onVolumeDestroyed(const ::std::string& volId) = 0;
};  // class IVoldListener

class IVoldListenerDefault : public IVoldListener {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onDiskCreated(const ::std::string& diskId, int32_t flags) override;
  ::android::binder::Status onDiskScanned(const ::std::string& diskId) override;
  ::android::binder::Status onDiskMetadataChanged(const ::std::string& diskId, int64_t sizeBytes, const ::std::string& label, const ::std::string& sysPath) override;
  ::android::binder::Status onDiskDestroyed(const ::std::string& diskId) override;
  ::android::binder::Status onVolumeCreated(const ::std::string& volId, int32_t type, const ::std::string& diskId, const ::std::string& partGuid) override;
  ::android::binder::Status onVolumeStateChanged(const ::std::string& volId, int32_t state) override;
  ::android::binder::Status onVolumeMetadataChanged(const ::std::string& volId, const ::std::string& fsType, const ::std::string& fsUuid, const ::std::string& fsLabel) override;
  ::android::binder::Status onVolumePathChanged(const ::std::string& volId, const ::std::string& path) override;
  ::android::binder::Status onVolumeInternalPathChanged(const ::std::string& volId, const ::std::string& internalPath) override;
  ::android::binder::Status onVolumeDestroyed(const ::std::string& volId) override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_VOLD_LISTENER_H_
