#ifndef AIDL_GENERATED_ANDROID_OS_I_VOLD_TASK_LISTENER_H_
#define AIDL_GENERATED_ANDROID_OS_I_VOLD_TASK_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/PersistableBundle.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/StrongPointer.h>

namespace android {

namespace os {

class IVoldTaskListener : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(VoldTaskListener)
  virtual ::android::binder::Status onStatus(int32_t status, const ::android::os::PersistableBundle& extras) = 0;
  virtual ::android::binder::Status onFinished(int32_t status, const ::android::os::PersistableBundle& extras) = 0;
};  // class IVoldTaskListener

class IVoldTaskListenerDefault : public IVoldTaskListener {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onStatus(int32_t status, const ::android::os::PersistableBundle& extras) override;
  ::android::binder::Status onFinished(int32_t status, const ::android::os::PersistableBundle& extras) override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_VOLD_TASK_LISTENER_H_
