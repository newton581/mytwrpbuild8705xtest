// File autogenerated by genseccomp.py - edit at your peril!!

#include <linux/filter.h>
#include <errno.h>

#include "seccomp/seccomp_bpfs.h"
const sock_filter mips64_app_zygote_filter[] = {
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5000, 0, 94),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5153, 47, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5089, 23, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5034, 11, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5008, 5, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5005, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5003, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5002, 87, 86), //read|write
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5004, 86, 85), //close
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5006, 85, 84), //fstat
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5031, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5023, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5020, 82, 81), //lseek|mmap|mprotect|munmap|brk|rt_sigaction|rt_sigprocmask|ioctl|pread64|pwrite64|readv|writev
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5028, 81, 80), //sched_yield|mremap|msync|mincore|madvise
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5032, 80, 79), //dup
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5057, 5, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5043, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5038, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5037, 76, 75), //nanosleep|getitimer|setitimer
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5042, 75, 74), //getpid|sendfile|socket|connect
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5056, 74, 73), //sendto|recvfrom|sendmsg|recvmsg|shutdown|bind|listen|getsockname|getpeername|socketpair|setsockopt|getsockopt|clone
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5077, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5070, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5062, 71, 70), //execve|exit|wait4|kill|uname
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5076, 70, 69), //fcntl|flock|fsync|fdatasync|truncate|ftruncate
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5080, 69, 68), //getcwd|chdir|fchdir
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5115, 11, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5105, 5, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5093, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5091, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5090, 64, 63), //fchmod
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5092, 63, 62), //fchown
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5103, 62, 61), //umask|gettimeofday|getrlimit|getrusage|sysinfo|times|ptrace|getuid|syslog|getgid
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5113, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5110, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5109, 59, 58), //geteuid|getegid|setpgid|getppid
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5111, 58, 57), //setsid
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5114, 57, 56), //getgroups
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5134, 5, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5132, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5122, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5120, 53, 52), //setresuid|getresuid|setresgid|getresgid|getpgid
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5130, 52, 51), //getsid|capget|capset|rt_sigpending|rt_sigtimedwait|rt_sigqueueinfo|rt_sigsuspend|sigaltstack
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5133, 51, 50), //personality
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5151, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5137, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5136, 48, 47), //statfs|fstatfs
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5150, 47, 46), //getpriority|setpriority|sched_setparam|sched_getparam|sched_setscheduler|sched_getscheduler|sched_get_priority_max|sched_get_priority_min|sched_rr_get_interval|mlock|munlock|mlockall|munlockall
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5152, 46, 45), //pivot_root
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5242, 23, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5200, 11, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5172, 5, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5157, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5155, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5154, 40, 39), //prctl
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5156, 39, 38), //setrlimit
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5158, 38, 37), //sync
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5194, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5178, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5173, 35, 34), //quotactl
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5193, 34, 33), //gettid|readahead|setxattr|lsetxattr|fsetxattr|getxattr|lgetxattr|fgetxattr|listxattr|llistxattr|flistxattr|removexattr|lremovexattr|fremovexattr|tkill
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5198, 33, 32), //futex|sched_setaffinity|sched_getaffinity|cacheflush
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5215, 5, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5211, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5208, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5206, 29, 28), //io_setup|io_destroy|io_getevents|io_submit|io_cancel|exit_group
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5209, 28, 27), //epoll_ctl
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5214, 27, 26), //rt_sigreturn|set_tid_address|restart_syscall
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5237, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5222, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5221, 24, 23), //fadvise64|timer_create|timer_settime|timer_gettime|timer_getoverrun|timer_delete
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5226, 23, 22), //clock_gettime|clock_getres|clock_nanosleep|tgkill
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5238, 22, 21), //waitid
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5297, 11, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5252, 5, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5247, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5244, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5243, 17, 16), //set_thread_area
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5246, 16, 15), //inotify_add_watch|inotify_rm_watch
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5251, 15, 14), //openat|mkdirat|mknodat|fchownat
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5279, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5271, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5267, 12, 11), //newfstatat|unlinkat|renameat|linkat|symlinkat|readlinkat|fchmodat|faccessat|pselect6|ppoll|unshare|splice|sync_file_range|tee|vmsplice
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5276, 11, 10), //getcpu|epoll_pwait|ioprio_set|ioprio_get|utimensat
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5295, 10, 9), //fallocate|timerfd_create|timerfd_gettime|timerfd_settime|signalfd4|eventfd2|epoll_create1|dup3|pipe2|inotify_init1|preadv|pwritev|rt_tgsigqueueinfo|perf_event_open|accept4|recvmmsg
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5316, 5, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5308, 3, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5301, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5298, 6, 5), //prlimit64
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5306, 5, 4), //syncfs|sendmmsg|setns|process_vm_readv|process_vm_writev
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5315, 4, 3), //getdents64|sched_setattr|sched_getattr|renameat2|seccomp|getrandom|memfd_create
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5318, 1, 0),
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5317, 2, 1), //execveat
BPF_JUMP(BPF_JMP|BPF_JGE|BPF_K, 5323, 1, 0), //membarrier|mlock2|copy_file_range|preadv2|pwritev2
BPF_STMT(BPF_RET|BPF_K, SECCOMP_RET_ALLOW),
};

const size_t mips64_app_zygote_filter_size = sizeof(mips64_app_zygote_filter) / sizeof(struct sock_filter);
