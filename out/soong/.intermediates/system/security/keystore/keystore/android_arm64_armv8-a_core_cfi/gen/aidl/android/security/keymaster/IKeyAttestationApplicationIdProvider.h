#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYMASTER_I_KEY_ATTESTATION_APPLICATION_ID_PROVIDER_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYMASTER_I_KEY_ATTESTATION_APPLICATION_ID_PROVIDER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <keystore/KeyAttestationApplicationId.h>
#include <utils/StrongPointer.h>

namespace android {

namespace security {

namespace keymaster {

class IKeyAttestationApplicationIdProvider : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(KeyAttestationApplicationIdProvider)
  virtual ::android::binder::Status getKeyAttestationApplicationId(int32_t uid, ::android::security::keymaster::KeyAttestationApplicationId* _aidl_return) = 0;
};  // class IKeyAttestationApplicationIdProvider

class IKeyAttestationApplicationIdProviderDefault : public IKeyAttestationApplicationIdProvider {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status getKeyAttestationApplicationId(int32_t uid, ::android::security::keymaster::KeyAttestationApplicationId* _aidl_return) override;

};

}  // namespace keymaster

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYMASTER_I_KEY_ATTESTATION_APPLICATION_ID_PROVIDER_H_
