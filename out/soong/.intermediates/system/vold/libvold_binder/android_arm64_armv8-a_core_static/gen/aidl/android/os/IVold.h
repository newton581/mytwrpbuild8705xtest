#ifndef AIDL_GENERATED_ANDROID_OS_I_VOLD_H_
#define AIDL_GENERATED_ANDROID_OS_I_VOLD_H_

#include <android-base/unique_fd.h>
#include <android/os/IVoldListener.h>
#include <android/os/IVoldTaskListener.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <memory>
#include <string>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace os {

class IVold : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(Vold)
  enum  : int32_t {
    ENCRYPTION_FLAG_NO_UI = 4,
    ENCRYPTION_STATE_NONE = 1,
    ENCRYPTION_STATE_OK = 0,
    ENCRYPTION_STATE_ERROR_UNKNOWN = -1,
    ENCRYPTION_STATE_ERROR_INCOMPLETE = -2,
    ENCRYPTION_STATE_ERROR_INCONSISTENT = -3,
    ENCRYPTION_STATE_ERROR_CORRUPT = -4,
    FSTRIM_FLAG_DEEP_TRIM = 1,
    MOUNT_FLAG_PRIMARY = 1,
    MOUNT_FLAG_VISIBLE = 2,
    PARTITION_TYPE_PUBLIC = 0,
    PARTITION_TYPE_PRIVATE = 1,
    PARTITION_TYPE_MIXED = 2,
    PASSWORD_TYPE_PASSWORD = 0,
    PASSWORD_TYPE_DEFAULT = 1,
    PASSWORD_TYPE_PATTERN = 2,
    PASSWORD_TYPE_PIN = 3,
    STORAGE_FLAG_DE = 1,
    STORAGE_FLAG_CE = 2,
    REMOUNT_MODE_NONE = 0,
    REMOUNT_MODE_DEFAULT = 1,
    REMOUNT_MODE_READ = 2,
    REMOUNT_MODE_WRITE = 3,
    REMOUNT_MODE_LEGACY = 4,
    REMOUNT_MODE_INSTALLER = 5,
    REMOUNT_MODE_FULL = 6,
    VOLUME_STATE_UNMOUNTED = 0,
    VOLUME_STATE_CHECKING = 1,
    VOLUME_STATE_MOUNTED = 2,
    VOLUME_STATE_MOUNTED_READ_ONLY = 3,
    VOLUME_STATE_FORMATTING = 4,
    VOLUME_STATE_EJECTING = 5,
    VOLUME_STATE_UNMOUNTABLE = 6,
    VOLUME_STATE_REMOVED = 7,
    VOLUME_STATE_BAD_REMOVAL = 8,
    VOLUME_TYPE_PUBLIC = 0,
    VOLUME_TYPE_PRIVATE = 1,
    VOLUME_TYPE_EMULATED = 2,
    VOLUME_TYPE_ASEC = 3,
    VOLUME_TYPE_OBB = 4,
    VOLUME_TYPE_STUB = 5,
  };
  virtual ::android::binder::Status setListener(const ::android::sp<::android::os::IVoldListener>& listener) = 0;
  virtual ::android::binder::Status monitor() = 0;
  virtual ::android::binder::Status reset() = 0;
  virtual ::android::binder::Status shutdown() = 0;
  virtual ::android::binder::Status onUserAdded(int32_t userId, int32_t userSerial) = 0;
  virtual ::android::binder::Status onUserRemoved(int32_t userId) = 0;
  virtual ::android::binder::Status onUserStarted(int32_t userId) = 0;
  virtual ::android::binder::Status onUserStopped(int32_t userId) = 0;
  virtual ::android::binder::Status addAppIds(const ::std::vector<::std::string>& packageNames, const ::std::vector<int32_t>& appIds) = 0;
  virtual ::android::binder::Status addSandboxIds(const ::std::vector<int32_t>& appIds, const ::std::vector<::std::string>& sandboxIds) = 0;
  virtual ::android::binder::Status onSecureKeyguardStateChanged(bool isShowing) = 0;
  virtual ::android::binder::Status partition(const ::std::string& diskId, int32_t partitionType, int32_t ratio) = 0;
  virtual ::android::binder::Status forgetPartition(const ::std::string& partGuid, const ::std::string& fsUuid) = 0;
  virtual ::android::binder::Status mount(const ::std::string& volId, int32_t mountFlags, int32_t mountUserId) = 0;
  virtual ::android::binder::Status unmount(const ::std::string& volId) = 0;
  virtual ::android::binder::Status format(const ::std::string& volId, const ::std::string& fsType) = 0;
  virtual ::android::binder::Status benchmark(const ::std::string& volId, const ::android::sp<::android::os::IVoldTaskListener>& listener) = 0;
  virtual ::android::binder::Status checkEncryption(const ::std::string& volId) = 0;
  virtual ::android::binder::Status moveStorage(const ::std::string& fromVolId, const ::std::string& toVolId, const ::android::sp<::android::os::IVoldTaskListener>& listener) = 0;
  virtual ::android::binder::Status remountUid(int32_t uid, int32_t remountMode) = 0;
  virtual ::android::binder::Status mkdirs(const ::std::string& path) = 0;
  virtual ::android::binder::Status createObb(const ::std::string& sourcePath, const ::std::string& sourceKey, int32_t ownerGid, ::std::string* _aidl_return) = 0;
  virtual ::android::binder::Status destroyObb(const ::std::string& volId) = 0;
  virtual ::android::binder::Status fstrim(int32_t fstrimFlags, const ::android::sp<::android::os::IVoldTaskListener>& listener) = 0;
  virtual ::android::binder::Status runIdleMaint(const ::android::sp<::android::os::IVoldTaskListener>& listener) = 0;
  virtual ::android::binder::Status abortIdleMaint(const ::android::sp<::android::os::IVoldTaskListener>& listener) = 0;
  virtual ::android::binder::Status mountAppFuse(int32_t uid, int32_t mountId, ::android::base::unique_fd* _aidl_return) = 0;
  virtual ::android::binder::Status unmountAppFuse(int32_t uid, int32_t mountId) = 0;
  virtual ::android::binder::Status fdeCheckPassword(const ::std::string& password) = 0;
  virtual ::android::binder::Status fdeRestart() = 0;
  virtual ::android::binder::Status fdeComplete(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status fdeEnable(int32_t passwordType, const ::std::string& password, int32_t encryptionFlags) = 0;
  virtual ::android::binder::Status fdeChangePassword(int32_t passwordType, const ::std::string& currentPassword, const ::std::string& password) = 0;
  virtual ::android::binder::Status fdeVerifyPassword(const ::std::string& password) = 0;
  virtual ::android::binder::Status fdeGetField(const ::std::string& key, ::std::string* _aidl_return) = 0;
  virtual ::android::binder::Status fdeSetField(const ::std::string& key, const ::std::string& value) = 0;
  virtual ::android::binder::Status fdeGetPasswordType(int32_t* _aidl_return) = 0;
  virtual ::android::binder::Status fdeGetPassword(::std::string* _aidl_return) = 0;
  virtual ::android::binder::Status fdeClearPassword() = 0;
  virtual ::android::binder::Status fbeEnable() = 0;
  virtual ::android::binder::Status mountDefaultEncrypted() = 0;
  virtual ::android::binder::Status initUser0() = 0;
  virtual ::android::binder::Status isConvertibleToFbe(bool* _aidl_return) = 0;
  virtual ::android::binder::Status mountFstab(const ::std::string& blkDevice, const ::std::string& mountPoint) = 0;
  virtual ::android::binder::Status encryptFstab(const ::std::string& blkDevice, const ::std::string& mountPoint) = 0;
  virtual ::android::binder::Status createUserKey(int32_t userId, int32_t userSerial, bool ephemeral) = 0;
  virtual ::android::binder::Status destroyUserKey(int32_t userId) = 0;
  virtual ::android::binder::Status addUserKeyAuth(int32_t userId, int32_t userSerial, const ::std::string& token, const ::std::string& secret) = 0;
  virtual ::android::binder::Status clearUserKeyAuth(int32_t userId, int32_t userSerial, const ::std::string& token, const ::std::string& secret) = 0;
  virtual ::android::binder::Status fixateNewestUserKeyAuth(int32_t userId) = 0;
  virtual ::android::binder::Status unlockUserKey(int32_t userId, int32_t userSerial, const ::std::string& token, const ::std::string& secret) = 0;
  virtual ::android::binder::Status lockUserKey(int32_t userId) = 0;
  virtual ::android::binder::Status prepareUserStorage(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t userSerial, int32_t storageFlags) = 0;
  virtual ::android::binder::Status destroyUserStorage(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t storageFlags) = 0;
  virtual ::android::binder::Status prepareSandboxForApp(const ::std::string& packageName, int32_t appId, const ::std::string& sandboxId, int32_t userId) = 0;
  virtual ::android::binder::Status destroySandboxForApp(const ::std::string& packageName, const ::std::string& sandboxId, int32_t userId) = 0;
  virtual ::android::binder::Status startCheckpoint(int32_t retry) = 0;
  virtual ::android::binder::Status needsCheckpoint(bool* _aidl_return) = 0;
  virtual ::android::binder::Status needsRollback(bool* _aidl_return) = 0;
  virtual ::android::binder::Status abortChanges(const ::std::string& device, bool retry) = 0;
  virtual ::android::binder::Status commitChanges() = 0;
  virtual ::android::binder::Status prepareCheckpoint() = 0;
  virtual ::android::binder::Status restoreCheckpoint(const ::std::string& device) = 0;
  virtual ::android::binder::Status restoreCheckpointPart(const ::std::string& device, int32_t count) = 0;
  virtual ::android::binder::Status markBootAttempt() = 0;
  virtual ::android::binder::Status supportsCheckpoint(bool* _aidl_return) = 0;
  virtual ::android::binder::Status supportsBlockCheckpoint(bool* _aidl_return) = 0;
  virtual ::android::binder::Status supportsFileCheckpoint(bool* _aidl_return) = 0;
  virtual ::android::binder::Status createStubVolume(const ::std::string& sourcePath, const ::std::string& mountPath, const ::std::string& fsType, const ::std::string& fsUuid, const ::std::string& fsLabel, ::std::string* _aidl_return) = 0;
  virtual ::android::binder::Status destroyStubVolume(const ::std::string& volId) = 0;
  virtual ::android::binder::Status openAppFuseFile(int32_t uid, int32_t mountId, int32_t fileId, int32_t flags, ::android::base::unique_fd* _aidl_return) = 0;
};  // class IVold

class IVoldDefault : public IVold {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status setListener(const ::android::sp<::android::os::IVoldListener>& listener) override;
  ::android::binder::Status monitor() override;
  ::android::binder::Status reset() override;
  ::android::binder::Status shutdown() override;
  ::android::binder::Status onUserAdded(int32_t userId, int32_t userSerial) override;
  ::android::binder::Status onUserRemoved(int32_t userId) override;
  ::android::binder::Status onUserStarted(int32_t userId) override;
  ::android::binder::Status onUserStopped(int32_t userId) override;
  ::android::binder::Status addAppIds(const ::std::vector<::std::string>& packageNames, const ::std::vector<int32_t>& appIds) override;
  ::android::binder::Status addSandboxIds(const ::std::vector<int32_t>& appIds, const ::std::vector<::std::string>& sandboxIds) override;
  ::android::binder::Status onSecureKeyguardStateChanged(bool isShowing) override;
  ::android::binder::Status partition(const ::std::string& diskId, int32_t partitionType, int32_t ratio) override;
  ::android::binder::Status forgetPartition(const ::std::string& partGuid, const ::std::string& fsUuid) override;
  ::android::binder::Status mount(const ::std::string& volId, int32_t mountFlags, int32_t mountUserId) override;
  ::android::binder::Status unmount(const ::std::string& volId) override;
  ::android::binder::Status format(const ::std::string& volId, const ::std::string& fsType) override;
  ::android::binder::Status benchmark(const ::std::string& volId, const ::android::sp<::android::os::IVoldTaskListener>& listener) override;
  ::android::binder::Status checkEncryption(const ::std::string& volId) override;
  ::android::binder::Status moveStorage(const ::std::string& fromVolId, const ::std::string& toVolId, const ::android::sp<::android::os::IVoldTaskListener>& listener) override;
  ::android::binder::Status remountUid(int32_t uid, int32_t remountMode) override;
  ::android::binder::Status mkdirs(const ::std::string& path) override;
  ::android::binder::Status createObb(const ::std::string& sourcePath, const ::std::string& sourceKey, int32_t ownerGid, ::std::string* _aidl_return) override;
  ::android::binder::Status destroyObb(const ::std::string& volId) override;
  ::android::binder::Status fstrim(int32_t fstrimFlags, const ::android::sp<::android::os::IVoldTaskListener>& listener) override;
  ::android::binder::Status runIdleMaint(const ::android::sp<::android::os::IVoldTaskListener>& listener) override;
  ::android::binder::Status abortIdleMaint(const ::android::sp<::android::os::IVoldTaskListener>& listener) override;
  ::android::binder::Status mountAppFuse(int32_t uid, int32_t mountId, ::android::base::unique_fd* _aidl_return) override;
  ::android::binder::Status unmountAppFuse(int32_t uid, int32_t mountId) override;
  ::android::binder::Status fdeCheckPassword(const ::std::string& password) override;
  ::android::binder::Status fdeRestart() override;
  ::android::binder::Status fdeComplete(int32_t* _aidl_return) override;
  ::android::binder::Status fdeEnable(int32_t passwordType, const ::std::string& password, int32_t encryptionFlags) override;
  ::android::binder::Status fdeChangePassword(int32_t passwordType, const ::std::string& currentPassword, const ::std::string& password) override;
  ::android::binder::Status fdeVerifyPassword(const ::std::string& password) override;
  ::android::binder::Status fdeGetField(const ::std::string& key, ::std::string* _aidl_return) override;
  ::android::binder::Status fdeSetField(const ::std::string& key, const ::std::string& value) override;
  ::android::binder::Status fdeGetPasswordType(int32_t* _aidl_return) override;
  ::android::binder::Status fdeGetPassword(::std::string* _aidl_return) override;
  ::android::binder::Status fdeClearPassword() override;
  ::android::binder::Status fbeEnable() override;
  ::android::binder::Status mountDefaultEncrypted() override;
  ::android::binder::Status initUser0() override;
  ::android::binder::Status isConvertibleToFbe(bool* _aidl_return) override;
  ::android::binder::Status mountFstab(const ::std::string& blkDevice, const ::std::string& mountPoint) override;
  ::android::binder::Status encryptFstab(const ::std::string& blkDevice, const ::std::string& mountPoint) override;
  ::android::binder::Status createUserKey(int32_t userId, int32_t userSerial, bool ephemeral) override;
  ::android::binder::Status destroyUserKey(int32_t userId) override;
  ::android::binder::Status addUserKeyAuth(int32_t userId, int32_t userSerial, const ::std::string& token, const ::std::string& secret) override;
  ::android::binder::Status clearUserKeyAuth(int32_t userId, int32_t userSerial, const ::std::string& token, const ::std::string& secret) override;
  ::android::binder::Status fixateNewestUserKeyAuth(int32_t userId) override;
  ::android::binder::Status unlockUserKey(int32_t userId, int32_t userSerial, const ::std::string& token, const ::std::string& secret) override;
  ::android::binder::Status lockUserKey(int32_t userId) override;
  ::android::binder::Status prepareUserStorage(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t userSerial, int32_t storageFlags) override;
  ::android::binder::Status destroyUserStorage(const ::std::unique_ptr<::std::string>& uuid, int32_t userId, int32_t storageFlags) override;
  ::android::binder::Status prepareSandboxForApp(const ::std::string& packageName, int32_t appId, const ::std::string& sandboxId, int32_t userId) override;
  ::android::binder::Status destroySandboxForApp(const ::std::string& packageName, const ::std::string& sandboxId, int32_t userId) override;
  ::android::binder::Status startCheckpoint(int32_t retry) override;
  ::android::binder::Status needsCheckpoint(bool* _aidl_return) override;
  ::android::binder::Status needsRollback(bool* _aidl_return) override;
  ::android::binder::Status abortChanges(const ::std::string& device, bool retry) override;
  ::android::binder::Status commitChanges() override;
  ::android::binder::Status prepareCheckpoint() override;
  ::android::binder::Status restoreCheckpoint(const ::std::string& device) override;
  ::android::binder::Status restoreCheckpointPart(const ::std::string& device, int32_t count) override;
  ::android::binder::Status markBootAttempt() override;
  ::android::binder::Status supportsCheckpoint(bool* _aidl_return) override;
  ::android::binder::Status supportsBlockCheckpoint(bool* _aidl_return) override;
  ::android::binder::Status supportsFileCheckpoint(bool* _aidl_return) override;
  ::android::binder::Status createStubVolume(const ::std::string& sourcePath, const ::std::string& mountPath, const ::std::string& fsType, const ::std::string& fsUuid, const ::std::string& fsLabel, ::std::string* _aidl_return) override;
  ::android::binder::Status destroyStubVolume(const ::std::string& volId) override;
  ::android::binder::Status openAppFuseFile(int32_t uid, int32_t mountId, int32_t fileId, int32_t flags, ::android::base::unique_fd* _aidl_return) override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_VOLD_H_
