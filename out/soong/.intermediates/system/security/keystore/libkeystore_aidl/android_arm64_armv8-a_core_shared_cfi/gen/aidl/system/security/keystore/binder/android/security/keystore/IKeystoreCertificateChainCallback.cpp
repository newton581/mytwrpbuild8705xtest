#include <android/security/keystore/IKeystoreCertificateChainCallback.h>
#include <android/security/keystore/BpKeystoreCertificateChainCallback.h>

namespace android {

namespace security {

namespace keystore {

IMPLEMENT_META_INTERFACE(KeystoreCertificateChainCallback, "android.security.keystore.IKeystoreCertificateChainCallback")

::android::IBinder* IKeystoreCertificateChainCallbackDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status IKeystoreCertificateChainCallbackDefault::onFinished(const ::android::security::keystore::KeystoreResponse&, const ::android::security::keymaster::KeymasterCertificateChain&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace keystore

}  // namespace security

}  // namespace android
#include <android/security/keystore/BpKeystoreCertificateChainCallback.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>

namespace android {

namespace security {

namespace keystore {

BpKeystoreCertificateChainCallback::BpKeystoreCertificateChainCallback(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<IKeystoreCertificateChainCallback>(_aidl_impl){
}

::android::binder::Status BpKeystoreCertificateChainCallback::onFinished(const ::android::security::keystore::KeystoreResponse& response, const ::android::security::keymaster::KeymasterCertificateChain& chain) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(response);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(chain);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* onFinished */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IKeystoreCertificateChainCallback::getDefaultImpl())) {
     return IKeystoreCertificateChainCallback::getDefaultImpl()->onFinished(response, chain);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

}  // namespace keystore

}  // namespace security

}  // namespace android
#include <android/security/keystore/BnKeystoreCertificateChainCallback.h>
#include <binder/Parcel.h>

namespace android {

namespace security {

namespace keystore {

::android::status_t BnKeystoreCertificateChainCallback::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* onFinished */:
  {
    ::android::security::keystore::KeystoreResponse in_response;
    ::android::security::keymaster::KeymasterCertificateChain in_chain;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_response);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_chain);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(onFinished(in_response, in_chain));
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

}  // namespace keystore

}  // namespace security

}  // namespace android
